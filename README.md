# Asgardian font awesome binding for LaTeX

This script is based on the work of Jan Hendrik Dolling.

1. Go to the awesome site https://fontawesome.com/ and download the free version of te destktop font.
2. Extract the font into this folder.
3. Run this script with the good parameters.
4. Copy the "fonts" folder and the style file into your project.
