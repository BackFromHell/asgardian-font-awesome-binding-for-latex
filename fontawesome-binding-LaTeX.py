#!/usr/bin/env python
# -*- coding: utf-8 -*-

#| aAwesomeBinding.py --- A LaTeX binding for font Awesome.
#|
#| By Thierry Leurent <thierry.leurent@asgardian.be>
#|
#| (C) 2019 Thierry Leurent
#|

#| Notice :
#|   Based on latex-fontawesome5 from Jan-Hendrik Dolling
#|      - https://github.com/JanHendrikDolling/latex-fontawesome5


import os
import sys
import shutil
import re
import json

#| Arguments and Configuration file.
from oslo_config import cfg
from oslo_config import types

#| Logging.
import logging
from logging.handlers import RotatingFileHandler


class Application(object):
    # | Templates for LaTex files.

    styHEADER_1 = r'''
% | Font Awesome LaTex binding.
% | LaTeX Style
% | Version 1.0.0 (2019/5/5)
% |
% | Original author:
% | Thierry Leurent <thierry.leurent@asgardian.be>
% |
% | License:
% | GPLv3.0

% Identify this package.
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{fontawesome5}
% Requirements to use.
\usepackage{fontspec}
% Configure a directory location for fonts(default: './fonts/')
\newcommand*{\fontdir}[1][./fonts/]{\def\@fontdir{#1}}
\fontdir

% | Font definitions.
%  --------------------------------------------
'''
    styHEADER_2 = r'''
\ProcessOptions\relax


% Generic command displaying an icon by its name.
\newcommand*{\faicon}[1]{{
\csname faicon@#1\endcsname
}}

% | FA Icon definition.
%  --------------------------------------------
'''

    templateFontFamily = '\\newfontfamily{\FA%(font)s}[Path=\@fontdir]{%(filename)s}\n'
    templateSymbolDefinition = '\expandafter\def\csname faicon@%(name)s\endcsname {\FA%(font)s\symbol{"%(symbol)s}}\n'
    templateDisplay = '  \\faDisplay{%(name)s}\n'

    texHEADER = r'''
% | Font Awesome LaTex binding demo cheat.
% | LaTeX Demo
% | Version 1.0.0 (2019/5/5)
% |
% | Original author:
% | Thierry Leurent <thierry.leurent@asgardian.be>
% |
% | License:
% | CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/

\documentclass[11pt,a4paper,sans]{article}
\usepackage{geometry}
\geometry{a4paper, top=3cm, bottom=3cm, left=3cm, right=3cm}
\usepackage[usenames, dvipsnames]{color}
\usepackage{multicol}

% Use the local binding
\usepackage{./fontawesome5}

% Display Icon and name
\newcommand*{\faDisplay}[1]{{
  \faicon{#1} #1\\
}}

\usepackage{fancyhdr}

\pagestyle{fancy}
\fancyhf{}
\rhead{Demo}
\lhead{Font Awesome binding for \LaTeX}
\rfoot{Page \thepage}

\setlength{\parindent}{0pt} % Stop paragraph indentation
\begin{document}
\begin{center}
This a demo document where you can see all 'icons'.
Use it to find the good one for your project.
\end{center}
\begin{multicols}{2}
'''

    texFOOTER = r'''
\end{multicols}

\end{document}
'''


    Version = "0.0.1"
    def __init__(self, Logger=None):
        #| Create an object logger to write logs.
        self.Logger = Logger or logging.getLogger(__name__)
        #| Add a second handler to write logs into the console.
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        self.Logger.addHandler(stream_handler)

        self.Configuration=self.ReadConfig(Version=self.Version)

        if (self.Configuration.LOGGING.Level != 'Notset'):
            #| My format.
            format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            #| Log to a file in mode 'append' and max size 1Mo
            logFile= os.path.join(self.Configuration.LOGGING.directory,
                                  self.Configuration.LOGGING.filename)

            if (self.Configuration.LOGGING.encoding is not None):
                file_handler = RotatingFileHandler(logFile,
                                                   mode=self.Configuration.LOGGING.mode,
                                                   maxBytes=self.Configuration.LOGGING.MaxBytes,
                                                   backupCount=self.Configuration.LOGGING.backupCount,
                                                   #encoding=self.Configuration.LOGGING.encoding,
                                                   delay=self.Configuration.LOGGING.delay)
            else:
                file_handler = RotatingFileHandler(logFile,
                                                   mode=self.Configuration.LOGGING.mode,
                                                   maxBytes=self.Configuration.LOGGING.MaxBytes,
                                                   backupCount=self.Configuration.LOGGING.backupCount,
                                                   delay=self.Configuration.LOGGING.delay)
            file_handler.setLevel(getattr(logging,
                                          self.Configuration.LOGGING.Level.upper()))
            file_handler.setFormatter(format)
            self.Logger.addHandler(file_handler)

            #print self.Configuration.LOGGING.Level
        self.Binding()

    def  ReadConfig(self, Version=0, ConfigFileName=None, LogFileName=None):
        opt_LOGGING_group = cfg.OptGroup(name='LOGGING',
                                         title='LOGGING Options'
        )
        if not (LogFileName):
            LogFileName = self.getLogFileName()
        LOGGING_opts = [
            cfg.StrOpt('directory',
                       default='./',
                       help='Where the log files must be saved.'),
            cfg.StrOpt('filename',
                       default=LogFileName,
                       help='The name of the log file.'),
            cfg.StrOpt('mode',
                       default='a',
                       help='By default, we append the file.'),
            cfg.IntOpt('MaxBytes',
                       default=1000000,
                       help='Max size of the log file (1Mo).'),
            cfg.IntOpt('backupCount',
                       default=0,
                       help='Max number of backup files.'),
            cfg.StrOpt('encoding',
                       default='None',
                       help='Use another encoding for the file.'),
            cfg.BoolOpt('delay',
                       default=False,
                       help='Open the file right now or wait to the first call.'),
            cfg.StrOpt('Level',
                       choices=['Notset',
                                 'Debug',
                                 'Info',
                                 'Warning',
                                 'Error',
                                 'Critical'],
                       default='Notset',
                       help='Set debug level.'),
        ]

        opt_FONT_group = cfg.OptGroup(name='FONT',
                                         title='FONT Options'
        )
        FONT_opts = [
            cfg.StrOpt('metadataPath',
                       default='metadata',
                       help='The folder of the metadata.'),
            cfg.StrOpt('metadataFileName',
                       default='icons.json',
                       help='The metadata file for icons definition.'),
            cfg.StrOpt('otfsPath',
                       default='otfs',
                       help='The otfs folder.'),
            cfg.StrOpt('fontPath',
                       default='fontawesome-free-5.8.1',
                       help='The main path of the font.'),
            cfg.StrOpt('LaTexFontPath',
                       default='fonts',
                       help='The folder where the are stored for LaTex .'),
            cfg.StrOpt('styFileName',
                       default='fontawesome5.sty',
                       help='The type file in LATEX to include into your document.'),
            cfg.StrOpt('texFileName',
                       default='fontawesome5-Sample.tex',
                       help='The sample file in LATEX.'),
        ]


        CONF = cfg.ConfigOpts()

        CONF.register_group(opt_LOGGING_group)
        CONF.register_opts(LOGGING_opts, opt_LOGGING_group)
        CONF.register_cli_opts(LOGGING_opts, opt_LOGGING_group)

        CONF.register_group(opt_FONT_group)
        CONF.register_opts(FONT_opts, opt_FONT_group)
        CONF.register_cli_opts(FONT_opts, opt_FONT_group)

        if not (ConfigFileName):
            ConfigFileName = self.getConfigFileName()
        try:
            CONF(version=Version,
                      default_config_files=[ConfigFileName]
            )
        except cfg.ConfigFilesNotFoundError:
            CONF(version=Version,
                      default_config_files=[]
            )
        except Exception as e:
            self.Logger.error("OSLO-Config: %s" % e)
            sys.exit("ERROR: %s" % e)
        except RuntimeError as e:
            self.Logger.error("OSLO-Config: %s" % e)
            sys.exit("ERROR: %s" % e)
        return CONF

    def getConfigFileName(self):
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), ('.').join(( os.path.splitext(os.path.basename(sys.argv[0]))[0],
                           'cfg')))
    def getLogFileName(self):
        return ('.').join(( os.path.splitext(os.path.basename(sys.argv[0]))[0],
                           'log'))

    def Binding(self):
        self.Logger.info("Binding")
        try:
            metadataFile = os.path.join(self.Configuration.FONT.fontPath, self.Configuration.FONT.metadataPath, self.Configuration.FONT.metadataFileName)
            otfsPath = os.path.join(self.Configuration.FONT.fontPath, self.Configuration.FONT.otfsPath)

            # | Create the destination font folder if not exist.
            if not os.path.exists(self.Configuration.FONT.LaTexFontPath):
                os.makedirs(self.Configuration.FONT.LaTexFontPath)

            styFile = open(self.Configuration.FONT.styFileName, "w")
            styFile.write(self.styHEADER_1)

            # | Create the FontFamily section into the style file. Copy and remane font files.
            for fontFileName in os.listdir(otfsPath) :
                reFontName = re.search("^((.*)\s((.*)-(.*)-(.*))\.otf)$", fontFileName)
                dstotfFontName = reFontName.group(1).replace(" ","")
                #print(dstotfFontName)
                if (reFontName.group(4) == "Brands"):
                    styFile.write(self.templateFontFamily %{
                        'font': reFontName.group(4), 'filename' : dstotfFontName
                    })
                else:
                    styFile.write(self.templateFontFamily %{
                        'font': reFontName.group(5), 'filename' : dstotfFontName
                    })

                shutil.copyfile(os.path.join(otfsPath, fontFileName),os.path.join(self.Configuration.FONT.LaTexFontPath,dstotfFontName))

            # | Read the meta data file and build the style file for LaTex.
            styFile.write(self.styHEADER_2)

            texFile = open(self.Configuration.FONT.texFileName, "w")
            texFile.write(self.texHEADER)
            with open(metadataFile, 'r') as json_data:
                    icons = json.load(json_data)
                    for icon in sorted(icons.keys()):
                        for style in icons[icon]['styles']:
                                if (len(icons[icon]['styles']) > 1):
                                    icon_name = icon + '-' + style
                                else:
                                    icon_name = icon
                                styFile.write(self.templateSymbolDefinition % {
                                        'name': icon_name, 'symbol': icons[icon]["unicode"].upper(), "font": style.capitalize()
                                })
                                texFile.write(self.templateDisplay %{
                                        'name': icon_name
                                })
            styFile.close()
            texFile.write(self.texFOOTER)
            texFile.close()


        except Exception as e:
            self.Logger.error("Binding: %s" % e)
            sys.exit("ERROR: %s" % e)
        except RuntimeError as e:
            self.Logger.error("Binding: %s" % e)
            sys.exit("ERROR: %s" % e)


if __name__ == "__main__":

    Application()
